## Debian configuration

`/etc/pam.d/common-session`

```
# add to the end if you need (auto create a home directory at initial login)
session optional        pam_mkhomedir.so skel=/etc/skel umask=077
```

`sudo apt install sssd`

`/etc/sssd/sssd.conf` : 

```
[sssd]
config_file_version = 2
## DO NOT declare the services, it conflict with systemd responder sockets in Debian 11
#services = nss, pam
domains = codeur

#[pam]
## Set the number of days for cache
#offline_credentials_expiration = 60

[domain/codeur]
cache_credentials = true
enumerate = true

id_provider = ldap
auth_provider = ldap

ldap_uri = ldap://localhost:1389
ldap_search_base = dc=codeur,dc=online
ldap_default_bind_dn = cn=system,dc=codeur,dc=online
ldap_default_authtok = __SYSUSER_PASSWORD__

ldap_id_use_start_tls = True
ldap_tls_reqcert = never
#ldap_tls_cacert = /etc/ssl/openldap/certs/cacert.pem
chpass_provider = ldap
ldap_chpass_uri = ldap://localhost:1389

ldap_search_timeout = 50
ldap_network_timeout = 2

# To prevent login with public key when account is locked
ldap_account_expire_policy = rhds
ldap_access_order = filter, expire

# OpenLDAP supports posixGroup, uncomment the following two lines
# to get group membership support
ldap_schema = rfc2307
ldap_group_member = memberUid
```

`systemctl start sssd`

### References

- https://wiki.archlinux.org/title/OpenLDAP#Installation
- https://wiki.archlinux.org/title/LDAP_authentication
- https://kifarunix.com/configure-sssd-for-openldap-client-authentication-on-debian-10-9/

## RFC2307bis for groups

Included in custom docker image

Allow to mix posixGroup and groupOfNames conjointly by setting posixGroup as AUXILIARY instead of STRUCTURAL

Coupled with the memberOf overlay to manage groups membership

https://ldapwiki.com/wiki/PAMSchemaModifications
https://github.com/jtyr/rfc2307bis
https://github.com/mesosphere-backup/docker-containers/blob/master/openldap/rfc2307bis/rfc2307bis.ldif

## UID numbers generation

LDAP does not support autoincrement, instead one can use a counter and an atomic (delete attr:value & add attr:value + 1)

https://rexconsulting.net/2011/11/28/ldap-protocol-uidnumber-html/
https://www.perlmonks.org/?node_id=457108

## SSH keys support

Customized to use MAY instead of MUST (set up in init.sh) : 

```
dn: cn=openssh-openldap,cn=schema,cn=config
objectClass: olcSchemaConfig
cn: openssh-openldap
olcAttributeTypes: {0}( 1.3.6.1.4.1.24552.500.1.1.1.13 NAME 'sshPublicKey'
  DESC 'MANDATORY: OpenSSH Public key'
  EQUALITY octetStringMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.40 )
olcObjectClasses: {0}( 1.3.6.1.4.1.24552.500.1.1.2.0 NAME 'ldapPublicKey'
  DESC 'MANDATORY: OpenSSH LPK objectclass'
  SUP top AUXILIARY 
  MAY ( sshPublicKey $ uid ) )
```

Test using `sss_ssh_authorizedkeys USERNAME`

Add support in `/etc/ssh/sshd_config` :

```
AuthorizedKeysCommand /usr/bin/sss_ssh_authorizedkeys
AuthorizedKeysCommandUser nobody
```

### References

- https://wiki.lereset.org/ateliers:serveurmail:ldap-ssh
- https://fy.blackhats.net.au/blog/html/2015/07/10/SSH_keys_in_ldap.html
- https://www.ossramblings.com/using-ldap-to-store-ssh-public-keys-with-sssd

## Notes

- Exports en empty /ldifs volume to disable default tree but not additional schema as LDAP_SKIP_DEFAULT_TREE also disable LDAP_EXTRA_SCHEMAS

## Overlays

### memberOf

MemberOf overlay for managing groups (auto attr update for entries)

To see the resulting `memberOf` attribute with ldapsearch, use `+` to request additional attributes

- https://tylersguides.com/guides/openldap-memberof-overlay/
- https://help.hcltechsw.com/domino/11.0.0/conf_usingldapsearchtoreturnoperationalattributes_t.html

### ppolicy

Password policy overlay to enforce policy and password hash mechanisms

- https://tobru.ch/openldap-password-policy-overlay/#:~:text=OpenLDAP%20has%20a%20dynamically%20loadable,a%20password%20and%20many%20more.


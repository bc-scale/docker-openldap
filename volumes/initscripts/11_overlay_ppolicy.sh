#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# Password policy overlay to enforce policy and password hash mechanisms
# https://tobru.ch/openldap-password-policy-overlay/#:~:text=OpenLDAP%20has%20a%20dynamically%20loadable,a%20password%20and%20many%20more.
info "Enable and configure password policy module..."

ldapadd -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: cn=module{1},cn=config
objectClass: olcModuleList
cn: module{1}
olcModuleLoad: ppolicy
EOF

ldapadd -Q -Y EXTERNAL -H "ldapi:///" > /dev/null <<EOF
dn: olcOverlay=ppolicy,olcDatabase={2}mdb,cn=config
objectClass: olcPPolicyConfig
olcOverlay: ppolicy
olcPPolicyDefault: cn=ppolicy,$LDAP_ROOT
olcPPolicyHashCleartext: TRUE
olcPPolicyUseLockout: TRUE
EOF

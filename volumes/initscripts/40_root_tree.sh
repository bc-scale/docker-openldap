#!/bin/bash
set -e && source /opt/bitnami/scripts/libopenldap.sh

# Initial tree structure
info "Creating initial tree structure"

ldapadd -D $LDAP_ADMIN_DN -w $LDAP_ADMIN_PASSWORD -H "ldapi:///" > /dev/null <<EOF
# LDAP root
dn: $LDAP_ROOT
objectClass: dcObject
objectClass: organization
dc: $LDAP_ROOT_NAME
o: $LDAP_ROOT_NAME

# OU for groups
dn: ou=groups,$LDAP_ROOT
objectClass: organizationalUnit
ou: groups
description: Root OU node for static groups

# Root OU node for users
dn: ou=users,$LDAP_ROOT
objectClass: organizationalUnit
ou: users
description: Root OU node for dynamic users
EOF

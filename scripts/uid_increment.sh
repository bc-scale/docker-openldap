#!/bin/bash
source .env

CURRENT_UID=2000
NEXT_UID=$(expr $CURRENT_UID + 1)

docker-compose exec -T -e LDAPTLS_REQCERT=never openldap ldapmodify -x -Z -H ldap://openldap:1389 -D uid=administrator,$LDAP_ROOT -w $ADMIN_PASSWORD <<EOF
dn: cn=uidNext,$LDAP_ROOT
changetype: modify
delete: uidNumber
uidNumber: $CURRENT_UID
-
add: uidNumber
uidNumber: $NEXT_UID
EOF
